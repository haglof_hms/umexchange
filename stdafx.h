// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once


#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500		// Target Windows 2000
#define _WIN32_WINNT 0x0500

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
//#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <SQLAPI.h> // main SQLAPI++ header

// XML handling
#import <msxml3.dll> //named_guids
#include <msxml2.h>

#include <vector>
#include <map>

#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions

#include "pad_hms_miscfunc.h"				// HMS_FuncLib
#include "pad_calculation_classes.h"	// HMS_FuncLib
#include "pricelistparser.h"		// HMS_FuncLib
//#include "xmllitebase.h"	

#include <afxdlgs.h>

#define ID_BRANDELS_EXCH_UB					2000
#define ID_NASLUNDS_EXCH_UB					2001
#define ID_OMF_FACTOR_EXCH_UB				2002

#define ID_BRANDELS_UB_110					110		// Volume Brandels Tall Norra Mindre
#define ID_BRANDELS_UB_111					111		// Volume Brandels Tall S�dra Mindre
#define ID_BRANDELS_UB_112					112		// Volume Brandels Tall Norra  BRG
#define ID_BRANDELS_UB_113					113		// Volume Brandels Tall S�dra  BRG
#define ID_BRANDELS_UB_114					114		// Volume Brandels Tall Norra BRG+KRG

#define ID_BRANDELS_UB_120					120		// Volume Brandels Gran Norra Mindre
#define ID_BRANDELS_UB_121					121		// Volume Brandels Gran S�dra Mindre
#define ID_BRANDELS_UB_122					122		// Volume Brandels Gran Norra  H�H
#define ID_BRANDELS_UB_123					123		// Volume Brandels Gran Norra H�H+KRG
#define ID_BRANDELS_UB_124					124		// Volume Brandels Gran Norra BRG
#define ID_BRANDELS_UB_125					125		// Volume Brandels Gran Norra H�H+KRG
#define ID_BRANDELS_UB_126					126		// Volume Brandels Gran S�dra KRG

#define ID_BRANDELS_UB_130					130		// Volume Brandels Gran Norra Mindre
#define ID_BRANDELS_UB_131					131		// Volume Brandels Gran S�dra Mindre
#define ID_BRANDELS_UB_132					132		// Volume Brandels Gran Norra  BRG
#define ID_BRANDELS_UB_133					133		// Volume Brandels Gran Norra H�H+KRG
#define ID_BRANDELS_UB_134					134		// Volume Brandels Gran Norra KRG

#define ID_NASLUNDS_UB_NL_2100			2100	// Volume "N�slunds" under bark Pine, Large, North
#define ID_NASLUNDS_UB_NS_2110			2110	// Volume "N�slunds" under bark Pine, Small, North
#define ID_NASLUNDS_UB_NL_2101			2101	// Volume "N�slunds" under bark Pine, Large, South
#define ID_NASLUNDS_UB_NS_2111			2111	// Volume "N�slunds" under bark Pine, Small, South

#define ID_NASLUNDS_UB_NL_2200			2200	// Volume "N�slunds" under bark Spruce, Large, North
#define ID_NASLUNDS_UB_NS_2210			2210	// Volume "N�slunds" under bark Spruce, Small, North
#define ID_NASLUNDS_UB_NL_2201			2201	// Volume "N�slunds" under bark Spruce, Large, South
#define ID_NASLUNDS_UB_NS_2211			2211	// Volume "N�slunds" under bark Spruce, Small, South

#define ID_NASLUNDS_UB_NL_2300			2300	// Volume "N�slunds" under bark Birch, Large, North
#define ID_NASLUNDS_UB_NS_2310			2310	// Volume "N�slunds" under bark Birch, Small, North
#define ID_NASLUNDS_UB_NL_2301			2301	// Volume "N�slunds" under bark Birch, Large, South
#define ID_NASLUNDS_UB_NS_2311			2311	// Volume "N�slunds" under bark Birch, Small, South


#define ID_OMF_FACTOR_UB_3100					3100	// Use "Omf�ringstal" per specie

#define ID_EXCHANGE_RO_1						1		// Rune Ollas utbytesber�kning med prislista
#define ID_EXCHANGE_RO_2						2		// Rune Ollas utbytesber�kning med medelpriser f�r sortiment

const LPCTSTR PROGRAM_NAME					= _T("UMExchange");				// Used for Languagefile, registry entries etc.; 070430 p�d


//////////////////////////////////////////////////////////////////////////////////////////
// Misc. functions

int SplitString(const CString& input,const CString& delimiter, CStringArray& results);
