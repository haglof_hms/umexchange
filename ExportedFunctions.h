#if !defined(AFX_EXPORTEDFUNCTIONS_H)
#define AFX_EXPORTEDFUNCTIONS_H

#include "StdAfx.h"

#define __BUILD

#ifdef __BUILD
#define DLL_BUILD extern __declspec(dllexport)
#else
#define DLL_BUILD extern __declspec(dllimport)
#endif

////////////////////////////////////////////////////////////////////////////
// exported functions
extern "C" 
{
//=====================================================================================
// Initialize the DLL, register the classes etc
//=====================================================================================
void DLL_BUILD InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &,vecINFO_TABLE &);

//=====================================================================================
// Returns a list of ALL bark-functions in this Module	
//=====================================================================================
void DLL_BUILD getVolumeFunctions_ub(vecUCFunctions &list);
void DLL_BUILD getVolumeFunctionList_ub(vecUCFunctionList &list);

//=====================================================================================
// Returns a list of ALL bark-functions in this Module	
//=====================================================================================
void DLL_BUILD getExchFunctions(vecUCFunctions &list);

//-------------------------------------------------------------------------------------
//	DoCalulate function; 060320 p�d
//-------------------------------------------------------------------------------------
BOOL DLL_BUILD doExchange(CTransaction_trakt_misc_data rec1,
												  vecTransactionDCLSTree &tree_list,
												  vecTransactionTreeAssort &tree_assort,
													vecTransactionTraktSetSpc &set_spc);

BOOL DLL_BUILD doExchangeSimple(CTransaction_trakt_misc_data rec1,					// In
																vecTransactionDCLSTree &tree_list,					// In
																vecTransactionTraktSetSpc &set_spc,					// In
															  vecTransactionTreeAssort &tree_assort_list,	// Out
																vecTransaction_exchange_simple &exch_data		// Out
																);
}

#endif