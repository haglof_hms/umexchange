#if !defined(AFX_EXCHANGEBASECLASS_H)
#define AFX_EXCHANGEBASECLASS_H

#include "StdAfx.h"

#include "CollectionSorter.h"

//--------------------------------------------------------------------------
// Added 2009-03-11 P�D
// Define this to separate calculate Price.
// If Pricelist set in M3To: Use Diamterclass_middle
// If Pricelist set in M3Fub: Use Diamterclass_middle - bark*2.0
// Uncomment to use Define

//#define __CALCULATE_ON_BARK_UNDER_BARK

//--------------------------------------------------------------------------

// Simple struct to temporarily hold infomartion on
// exchange volume for diamterclass and specie; 070525 p�d
struct _dcls_classes
{
	int traktid;	// To identify each tree
	int treeid;		// To identify each tree
	double dcls;
	double exch_diam;
	int spcid;
	double volume;
	double price;
	double dbh;
	int price_in;
	long nNumOfSpc;

	_dcls_classes(void)
	{
		traktid = 0;
		treeid = 0;
		dcls = 0.0;
		exch_diam = 0.0;
		spcid = -1;
		volume = 0.0;
		price = 0.0;
		dbh = 0.0;
		price_in = -1;
		nNumOfSpc = 0;
	}
	_dcls_classes(int _traktid,int _treeid,double _dcls,double _min_diam,int _spcid,
								double _volume,double _price,double _dbh,int _price_in,long num_of_trees)
	{
		traktid = _traktid;
		treeid = _treeid;
		dcls = _dcls;
		exch_diam = _min_diam;
		spcid = _spcid;
		volume = _volume;
		price = _price;
		dbh = _dbh;
		price_in = _price_in;
		nNumOfSpc = num_of_trees;
	}

};

////////////////////////////////////////////////////////////////////////
// CDCLSCompare_ExhDiam; comparer for sorting _dcls_classes
class CDCLSCompare_ExhDiam : public IComparer<_dcls_classes *>
{
public:
	CDCLSCompare_ExhDiam() {}
	virtual ~CDCLSCompare_ExhDiam() {}

	bool IsABigger() const
	{
		return (m_cmpA->exch_diam > m_cmpB->exch_diam);
	}
	bool IsBBigger() const
	{
		return (m_cmpA->exch_diam < m_cmpB->exch_diam);
	}
	bool Equal() const
	{
		return (m_cmpA->exch_diam == m_cmpB->exch_diam);
	}
};

////////////////////////////////////////////////////////////////////////
// CDCLSCompare_SpcID; comparer for sorting _dcls_classes
class CDCLSCompare_SpcID : public IComparer<_dcls_classes *>
{
public:
	CDCLSCompare_SpcID() {}
	virtual ~CDCLSCompare_SpcID() {}

	bool IsABigger() const
	{
		return (m_cmpA->spcid > m_cmpB->spcid);
	}
	bool IsBBigger() const
	{
		return (m_cmpA->spcid < m_cmpB->spcid);
	}
	bool Equal() const
	{
		return (m_cmpA->spcid == m_cmpB->spcid);
	}
};


typedef	CList<_dcls_classes *,_dcls_classes *> DCLSClasses;
typedef CCollectionSorter<_dcls_classes *,_dcls_classes *> DCLSClassesSorter;


typedef std::vector<double> vecDouble;

////////////////////////////////////////////////////////////////////////
// CDCLSCompare_PrlAss; comparer for sorting CTransaction_assort
class CDCLSCompare_PrlAss : public IComparer<CTransaction_assort *>
{
public:
	CDCLSCompare_PrlAss() {}
	virtual ~CDCLSCompare_PrlAss() {}

	bool IsABigger() const
	{
		return (m_cmpA->getMinDiam() < m_cmpB->getMinDiam());
	}
	bool IsBBigger() const
	{
		return (m_cmpA->getMinDiam() > m_cmpB->getMinDiam());
	}
	bool Equal() const
	{
		return (m_cmpA->getMinDiam() == m_cmpB->getMinDiam());
	}
};

typedef CList<CTransaction_assort *,CTransaction_assort *> ListPrlAss;
typedef CCollectionSorter<CTransaction_assort *,CTransaction_assort *> PrlAssSorter;

////////////////////////////////////////////////////////////////////////
// CExchangeBase; holds basic information for derived classes to
// calculate exchange; 070525 p�d
class CExchangeBase 
{
	vecTransactionDCLSTree m_vecDCLSTrees;

	vecTransactionSpecies m_vecPricelistSpecies;	// Holds information on Species in pricelist
	vecTransactionAssort m_vecPricelistAssort;		// Holds information on assortments, set in Pricelist
	vecTransactionTraktSetSpc m_vecSetSpc;				// Holds information on e.g. selected qualitydescription
	// Pricelist vectors; 070529 p�d
	vecTransactionDiameterclass m_vecPrlDCLS;			// Holds information on diamterclasses in pricelist, for specie
	vecTransactionPrlData m_vecPrlPriceForSpc;		// Holds information on price per dcls for specie
	vecTransactionPrlData m_vecPrlQDescForSpc;		// Holds information on percentage of price per quality for specie

	int m_nActiveSpcID;
	int m_nNumOfQDescPerSpc;
	CStringArray m_sarrQDescNames;
protected:
	xmllitePricelistParser *m_pPrlParser;
	BOOL m_bParserOK;

	CTransaction_trakt_misc_data m_recMiscData;	// Holds e.g. XML pricelist file
	vecTransactionTreeAssort m_vecTreeAssorts;

	void loadPricelistInformation(void);

	void loadPrlPriceAndQescForSpc(int spc_id);
	
	vecTransactionAssort& getPricelistAssortment(void)
	{
		return m_vecPricelistAssort;
	}

	vecTransactionSpecies& getPricelistSpecies(void)
	{
		return m_vecPricelistSpecies;
	}

	void setM3FubForDCLSTreesList(CTransaction_dcls_tree &tree,double m3fub);

	vecTransactionDCLSTree& getTrees(void)
	{
		return m_vecDCLSTrees;
	}

	vecTransactionTraktSetSpc& getSetSpc(void)
	{
		return m_vecSetSpc;
	}

	vecTransactionDiameterclass& getPrlDCLS(void)
	{
		return m_vecPrlDCLS;
	}

	vecTransactionPrlData& getPrlPrices(void)
	{
		return m_vecPrlPriceForSpc;
	}

	vecTransactionPrlData& getPrlQDescs(void)
	{
		return m_vecPrlQDescForSpc;
	}

	UINT getSetSpcQDescForSpc(int spc_id);

	double calculatePrice(double dbh);

	CString getSpcName(int id);

	DCLSClasses m_listDCLSClasses_ExchDiam;
	DCLSClassesSorter m_listDCLSClassesSorter;

	double getClassMid(CTransaction_dcls_tree& v)
	{
		double fDCLS = (v.getDCLS_to() - v.getDCLS_from());
		double fMid = fDCLS / 2.0;
		return (v.getDCLS_from() + fMid)*10.0;	// From (cm) to (mm)
	}

	int m_nTimberpriceIn;	// Added 080424 p�d, "Timmerpris 1 = m3fub 2 = m3to

	// Added 2008-10-17 p�d
	double getM3FubToM3TO(int spc_id);

	vecTransaction_exchange_simple m_vecExchSimpleData;	// Added 100409 p�d

public:
	CExchangeBase(void);

	CExchangeBase(CTransaction_trakt_misc_data,
								vecTransactionDCLSTree &,
								vecTransactionTreeAssort&,
								vecTransactionTraktSetSpc&);

	CExchangeBase(CTransaction_trakt_misc_data,
								vecTransactionDCLSTree &,
								vecTransactionTraktSetSpc&,
								vecTransactionTreeAssort&,
								vecTransaction_exchange_simple&);

	virtual ~CExchangeBase(void) { if (m_pPrlParser) delete m_pPrlParser; }

	vecTransactionTreeAssort& getTreeAssortData(void)
	{
		return m_vecTreeAssorts;
	}

	vecTransactionDCLSTree& getTreeDCLS(void)
	{
		return m_vecDCLSTrees;
	}

	vecTransaction_exchange_simple& getExchSimpleData()
	{
		return m_vecExchSimpleData;
	}
};

////////////////////////////////////////////////////////////////////////
// CExchangeRO; Run Ollas exchange calculation
class CExchangeRO : public CExchangeBase
{
	//private:
	double calcExchange(double dbh,double hgt,double min_diam);
	double calcGFall(double dbh,double hgt,double min_diam);	// "Fallande l�ngder"
	double calcG3m(double dbh,double hgt,double min_diam);	// "3 meters l�ngder"

protected:
	double getExchange(double dbh,double hgt,double min_diam,int gval,int sel);

public:
	CExchangeRO(void);

	CExchangeRO(CTransaction_trakt_misc_data,
							vecTransactionDCLSTree &,
							vecTransactionTreeAssort&,
							vecTransactionTraktSetSpc&);

	CExchangeRO(CTransaction_trakt_misc_data,
							vecTransactionDCLSTree &,
							vecTransactionTraktSetSpc&,
							vecTransactionTreeAssort&,
							vecTransaction_exchange_simple&);

	~CExchangeRO(void);

	void doExchangeCalculation(void);
	void doExchangeCalculationSimple(void);

};

#endif