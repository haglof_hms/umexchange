#include "StdAfx.h"

#include "ExportedFunctions.h"

#include "ExchangeBaseClass.h"	// Holds ALL excxhange classes; 070525 p�d

////////////////////////////////////////////////////////////////////////////
// exported functions


//=========================================================================
// Returns a list of ALL exchange functions in this module; 070430 p�d
void DLL_BUILD getExchFunctions(vecUCFunctions &list)
{
	list.push_back(UCFunctions(ID_EXCHANGE_RO_1,_T("Matrisprislista" )));
	list.push_back(UCFunctions(ID_EXCHANGE_RO_2,_T("Medelprislista" )));
}

//=========================================================================
// Do exchange calculations 
BOOL DLL_BUILD doExchange(CTransaction_trakt_misc_data rec1,
												  vecTransactionDCLSTree &tree_list,
													vecTransactionTreeAssort &tree_assort,
													vecTransactionTraktSetSpc &set_spc)
{

	CExchangeRO *pRO = new CExchangeRO(rec1,tree_list,tree_assort,set_spc);
	if (pRO != NULL)
	{
		pRO->doExchangeCalculation();
		tree_assort = pRO->getTreeAssortData();
		tree_list = pRO->getTreeDCLS();

		delete pRO;
	}


	return TRUE;
}

//=========================================================================
// Do exchange calculations 
BOOL DLL_BUILD doExchangeSimple(CTransaction_trakt_misc_data rec1,
																vecTransactionDCLSTree &tree_list,
																vecTransactionTraktSetSpc &set_spc,
	 														  vecTransactionTreeAssort &tree_assort_list,
																vecTransaction_exchange_simple &exch_data)
{

	CExchangeRO *pRO = new CExchangeRO(rec1,tree_list,set_spc,tree_assort_list,exch_data);
	if (pRO != NULL)
	{
		pRO->doExchangeCalculationSimple();
		exch_data = pRO->getExchSimpleData();
		tree_assort_list = pRO->getTreeAssortData();


		delete pRO;
	}

	return TRUE;
}
