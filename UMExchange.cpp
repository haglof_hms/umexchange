// UMExchange.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"

#include "Resource.h"

#include "ExportedFunctions.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "afxdllx.h"    // standard MFC Extension DLL routines

static AFX_EXTENSION_MODULE UMExchangeDLL = { NULL, NULL };

HINSTANCE hInst = NULL;


extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
	
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UMExchangeDLL, hInstance))
			return 0;
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		// Terminate the library before destructors are called
		AfxTermExtensionModule(UMExchangeDLL);
	}
	hInst = hInstance;
	return 1;   // ok
}

//=====================================================================================
// Initialize the DLL, register the classes etc
//=====================================================================================
void DLL_BUILD InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &idx,vecINFO_TABLE &vecInfo)
{
	new CDynLinkLibrary(UMExchangeDLL);
	CString sLangFN;
	CString sVersion;
	CString sCopyright;
	CString sCompany;

	// Setup the searchpath and name of the program.
	// This information is used e.g. in setting up the
	// Language filename in an OpenSuite() function; 051214 p�d
	sLangFN.Format((const wchar_t("%s%s")),getLanguageDir(),PROGRAM_NAME);

	// Form view to enter data
/*
	app->AddDocTemplate(new CMultiDocTemplate(ID_VIEW_5000,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIStandEntryFormFrame),
			RUNTIME_CLASS(CMDITabbedView)));
	idx.push_back(INDEX_TABLE(ID_VIEW_5000,suite,sLangFN,TRUE));
*/

	
	// Get version information; 060803 p�d

	const LPCTSTR VER_NUMBER							= _T("FileVersion");
	const LPCTSTR VER_COMPANY						= _T("CompanyName");
	const LPCTSTR VER_COPYRIGHT					= _T("LegalCopyright");

	sVersion		= getVersionInfo(hInst,VER_NUMBER);
	sCopyright	= getVersionInfo(hInst,VER_COPYRIGHT);
	sCompany		= getVersionInfo(hInst,VER_COMPANY);
	vecInfo.push_back(INFO_TABLE(-999,2 /* User module */,
															 (TCHAR*)sLangFN.GetBuffer(),
															 (TCHAR*)sVersion.GetBuffer(),
															 (TCHAR*)sCopyright.GetBuffer(),
															 (TCHAR*)sCompany.GetBuffer()));
		// Check if there's a connection set; 070329 p�d
	if (getIsDBConSet() == 1)
	{
		CString sPath;
/*
		sPath.Format("%s\\%s\\%s",getProgDir(),SUBDIR_SCRIPTS,ESTI_TABLES);
		runSQLScriptFile(sPath,TBL_TRAKT);

		sPath.Format("%s\\%s\\%s",getProgDir(),SUBDIR_SCRIPTS,ESTI_MISC_TABLES);
		runSQLScriptFile(sPath,TBL_TRAKT_TYPE);
*/
	}

}
