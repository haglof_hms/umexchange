#include "StdAfx.h"

#include <vector>
#include <algorithm>
//#include <functional>  

#include "ExchangeBaseClass.h"

// Function to sort m_vecTreeAssorts, makin' sure we get
// min diameters in ascending order; 071127 p�d
bool SortData(CTransaction_tree_assort& lhs, CTransaction_tree_assort& rhs)
{
	return lhs.getDCLS() > rhs.getDCLS();
}


////////////////////////////////////////////////////////////////////////
// CExchangeBase; holds basic information for derived classes to
// calculate exchange; 070525 p�d

CExchangeBase::CExchangeBase(void)
{
	m_vecDCLSTrees.clear();
	m_vecTreeAssorts.clear();
	m_vecExchSimpleData.clear();
}

CExchangeBase::CExchangeBase(CTransaction_trakt_misc_data rec1,
														 vecTransactionDCLSTree &tree_list,
														 vecTransactionTreeAssort &tree_assort_list,
														 vecTransactionTraktSetSpc &set_spc)
{
	m_recMiscData	= rec1;
	m_vecDCLSTrees	= tree_list;
	m_vecTreeAssorts = tree_assort_list;
	m_vecSetSpc = set_spc;

	m_vecExchSimpleData.clear();	// Not used

	m_bParserOK = FALSE;

	m_pPrlParser = new xmllitePricelistParser();
	if (m_pPrlParser != NULL)
	{
		if (m_pPrlParser->loadStream(m_recMiscData.getXMLPricelist()))
		{
			m_bParserOK = TRUE;
		}
	}

	// Get assortments declared in Pricelist (m_recMiscData); 070525 p�d
	loadPricelistInformation();
}

CExchangeBase::CExchangeBase(CTransaction_trakt_misc_data rec1,
														 vecTransactionDCLSTree &tree_list,
														 vecTransactionTraktSetSpc &set_spc,
														 vecTransactionTreeAssort &tree_assort_list,
														 vecTransaction_exchange_simple &exch_data)
{
	m_recMiscData	= rec1;
	m_vecDCLSTrees	= tree_list;
	m_vecTreeAssorts = tree_assort_list;
	m_vecSetSpc = set_spc;

	m_vecExchSimpleData = exch_data;

	m_bParserOK = FALSE;

	m_pPrlParser = new xmllitePricelistParser();
	if (m_pPrlParser != NULL)
	{
		if (m_pPrlParser->loadStream(m_recMiscData.getXMLPricelist()))
		{
			m_bParserOK = TRUE;
		}
	}

	// Get assortments declared in Pricelist (m_recMiscData); 070525 p�d
	loadPricelistInformation();
}

// PROTECTED
void CExchangeBase::loadPricelistInformation(void)
{
	if (m_bParserOK)
	{
		m_pPrlParser->getHeaderPriceIn(&m_nTimberpriceIn);
		//---------------------------------------------------------------------------------
		// Get species; 070528 p�d
		m_pPrlParser->getSpeciesInPricelistFile(m_vecPricelistSpecies);
		//---------------------------------------------------------------------------------
		// Get assortments per specie; 070525 p�d
		m_pPrlParser->getAssortmentPerSpecie(m_vecPricelistAssort);
	}
}

void CExchangeBase::loadPrlPriceAndQescForSpc(int spc_id)
{
	if (m_bParserOK)
	{
		//---------------------------------------------------------------------------------
		// Get dimeterclasses for pricelist; 070529 p�d
		m_pPrlParser->getDCLSForSpecie(spc_id,m_vecPrlDCLS);
		//---------------------------------------------------------------------------------
		// Get price per dcls per specie; 070529 p�d
		m_pPrlParser->getPriceForSpecie(spc_id,m_vecPrlPriceForSpc);
		//---------------------------------------------------------------------------------
		// Get name of Quality descriptions per specie; 070529 p�d
		m_pPrlParser->getQualDescNameForSpecie(spc_id,m_sarrQDescNames);
		//---------------------------------------------------------------------------------
		// Get percentage of price per quality for specie; 070529 p�d
		m_pPrlParser->getQualDescPercentForSpecie(spc_id,m_vecPrlQDescForSpc);
		m_nActiveSpcID = spc_id;
	}	// if (m_bParserOK)

}

UINT CExchangeBase::getSetSpcQDescForSpc(int spc_id)
{
	CString S;
	if (m_vecSetSpc.size() > 0)
	{
		for (UINT i = 0;i < m_vecSetSpc.size();i++)
		{
			if (m_vecSetSpc[i].getSpcID() == spc_id)
				return m_vecSetSpc[i].getSelQDescIndex();
		}
	}
	return 0;
}

double CExchangeBase::getM3FubToM3TO(int spc_id)
{
	if (m_vecSetSpc.size() > 0)
	{
		for (UINT i = 0;i < m_vecSetSpc.size();i++)
		{
			if (m_vecSetSpc[i].getSpcID() == spc_id)
				return m_vecSetSpc[i].getM3FubToM3To();
		}
	}
	return 0;
}

void CExchangeBase::setM3FubForDCLSTreesList(CTransaction_dcls_tree &tree,double m3fub)
{
	for (UINT i = 0;i < m_vecDCLSTrees.size();i++)
	{
		if (tree.getPlotID() == m_vecDCLSTrees[i].getPlotID() &&
				tree.getTraktID() == m_vecDCLSTrees[i].getTraktID() &&
				tree.getTreeID() == m_vecDCLSTrees[i].getTreeID() &&
				tree.getSpcID() == m_vecDCLSTrees[i].getSpcID() &&
				tree.getDCLS_from() == m_vecDCLSTrees[i].getDCLS_from() &&
				tree.getDCLS_to() == m_vecDCLSTrees[i].getDCLS_to())
				m_vecDCLSTrees[i].setM3fub(m3fub);
	}
}

double CExchangeBase::calculatePrice(double dbh)
{
	CString S;
	double fStartDiam = 0.0;
	double fNextDiam = 0.0;
	double fDBH = dbh; // * 10.0;	// From (cm) to (mm)
	int nNumOfQualitiesPerQDesc = 0;
	int nStart = 0;
	int nEnd = 0;
	double fPercent = 0.0;
	double fPrice = 0.0;
	UINT nDCLSIndex = 0;
	UINT nQDescIndex = 0;
	int nSpcID = -1;
	std::map<int,double> mapDouble;
	BOOL bFound = FALSE;
	if (m_vecPrlDCLS.size() > 0)
	{
		for (nDCLSIndex = 0;nDCLSIndex < m_vecPrlDCLS.size();nDCLSIndex++)
		{
			CTransaction_diameterclass dcls = m_vecPrlDCLS[nDCLSIndex];
			fStartDiam = dcls.getStartDiam() * 1.0;
			fNextDiam = (dcls.getStartDiam() + dcls.getDiamClass()) * 1.0;

			// If diamter is less than smallest diameterclass
			// return first diamterclass for diameter; 070529 p�d
			if (fDBH < fStartDiam && nDCLSIndex == 0 /* First DCLS */)
			{
				bFound = TRUE;
				break;
			}

			if (fDBH >= fStartDiam && fDBH < fNextDiam)
			{
				bFound = TRUE;
				break;
			}

		}
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		// If diamter isn't within the limits of Diamterclasses in pricelist
		// set nDCLSIndex to last diameter in pricelist;: 070608 p�d
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		
		if (!bFound)
		{
			nDCLSIndex = m_vecPrlDCLS.size() - 1;
		}
		// Make sure we found something; 070529 p�d
		if (nDCLSIndex >= 0 && m_vecPrlPriceForSpc.size() > 0 && m_vecPrlQDescForSpc.size() > 0)
		{
			nQDescIndex = getSetSpcQDescForSpc(m_nActiveSpcID);
			// Number of qualities per qualitydescriptions; 070529 p�d
			nNumOfQualitiesPerQDesc = m_sarrQDescNames.GetCount(); //(m_vecPrlQDescForSpc.size()/m_sarrQDescNames.GetCount());
			nStart = (nQDescIndex)*nNumOfQualitiesPerQDesc;
			nEnd = nStart + nNumOfQualitiesPerQDesc;
			// Holds Percentage for DCLS from Qualitydescription selected; 070529 p�d
			mapDouble.clear();
			int nMapCounter = 0;
			// Make sure we don't get out of bounds; 091016 p�d
			//if (m_vecPrlQDescForSpc.size() >= nEnd)
			//{
				for (UINT iii = 0;iii < m_vecPrlQDescForSpc.size();iii++)
				{
					CTransaction_prl_data data =	m_vecPrlQDescForSpc[iii];
/*
			S.Format(_T("CExchangeBase::calculatePrice\nm_nActiveSpcID %d\ndata.getIdentifer() %d\nnQDescIndex %d"),
					m_nActiveSpcID,data.getIdentifer(),nQDescIndex);
			AfxMessageBox(S);
*/
					if (data.getIdentifer() == nQDescIndex)
					{
						if (data.getInts().size() > 0)
						{
							if (nDCLSIndex < data.getInts().size() && nDCLSIndex >= 0)
							{
								fPercent = data.getInts()[nDCLSIndex]/100.0;
/*
				S.Format(_T("A CExchangeBase::calculatePrice\nm_nActiveSpcID %d\nfDBH %f\nm_vecPrlQDescForSpc.size() %d\nnQDescIndex %d\nnNumOfQualitiesPerQDesc %d\nnStart %d\nnEnd %d\nPercent %f"),
						m_nActiveSpcID,fDBH,m_vecPrlQDescForSpc.size(),nQDescIndex,nNumOfQualitiesPerQDesc,nStart,nEnd,fPercent);
*/

							}
							else if (nDCLSIndex == data.getInts().size())
							{
								fPercent = data.getInts()[data.getInts().size()-1]/100.0;
/*
				S.Format(_T("B CExchangeBase::calculatePrice\nm_nActiveSpcID %d\nfDBH %f\nm_vecPrlQDescForSpc.size() %d\nnQDescIndex %d\nnNumOfQualitiesPerQDesc %d\nnStart %d\nnEnd %d\nPercent %f"),
						m_nActiveSpcID,fDBH,m_vecPrlQDescForSpc.size(),nQDescIndex,nNumOfQualitiesPerQDesc,nStart,nEnd,fPercent);
*/
							}
							mapDouble[nMapCounter] = fPercent;
							nMapCounter++;
//					AfxMessageBox(S);
						}	// if (data.getInts().size() > 0)
					}
				}	// for (UINT iii = nStart;iii < nEnd;iii++)
			//}	// if (m_vecPrlQDescForSpc.size() > nEnd)

			fPrice = 0.0;
			for (UINT i = 0;i < m_vecPrlPriceForSpc.size();i++)
			{
				CTransaction_prl_data data =	m_vecPrlPriceForSpc[i];

				//if (i < mapDouble.size())
					fPercent = mapDouble[i];
				//else
				//fPercent = 0.0;
				
				if (data.getInts().size() > 0)
				{
					if (nDCLSIndex < data.getInts().size() && nDCLSIndex >= 0)
					{
						fPrice += data.getInts()[nDCLSIndex]*1.0*fPercent;
/*
			S.Format(_T("A CExchangeBase::calculatePrice\nm_nActiveSpcID %d\nfDBH %f\nm_vecPrlQDescForSpc.size() %d\nnQDescIndex %d\nnNumOfQualitiesPerQDesc %d\nnStart %d\nnEnd %d\ndata.getInts()[nDCLSIndex]*1.0 %f\nPercent %f"),
					m_nActiveSpcID,fDBH,m_vecPrlQDescForSpc.size(),nQDescIndex,nNumOfQualitiesPerQDesc,nStart,nEnd,data.getInts()[nDCLSIndex]*1.0,fPercent);
*/
					}
					else if (nDCLSIndex == data.getInts().size())
					{
						fPrice += data.getInts()[nDCLSIndex-1]*1.0*fPercent;
/*
			S.Format(_T("B CExchangeBase::calculatePrice\nm_nActiveSpcID %d\nfDBH %f\nm_vecPrlQDescForSpc.size() %d\nnQDescIndex %d\nnNumOfQualitiesPerQDesc %d\nnStart %d\nnEnd %d\ndata.getInts()[nDCLSIndex-1]*1.0 %f\nPercent %f"),
					m_nActiveSpcID,fDBH,m_vecPrlQDescForSpc.size(),nQDescIndex,nNumOfQualitiesPerQDesc,nStart,nEnd,data.getInts()[nDCLSIndex-1]*1.0,fPercent);
*/
					}
					//AfxMessageBox(S);
				}	// if (data.getInts().size() > 0)

			}	// for (UINT i = 0;i < m_vecPrlPriceForSpc.size();i++)

			return fPrice;		
				
		}	// if (nDCLSIndex >= 0 && m_vecPrlPriceForSpc.size() > 0 && m_vecPrlQDescForSpc.size() > 0)

	}	// if (m_vecPrlDCLS.size() > 0)
	return 0.0;
}

// Get name of specie, based on spcie id, from Pricelist; 070528 p�d
CString CExchangeBase::getSpcName(int id)
{
	if (m_vecPricelistSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecPricelistSpecies.size();i++)
		{
			if (m_vecPricelistSpecies[i].getSpcID() == id)
				return m_vecPricelistSpecies[i].getSpcName();
		}
	}
	return _T("");
}

////////////////////////////////////////////////////////////////////////
// CExchangeRO; Run Ollas exchange calculation

CExchangeRO::CExchangeRO(void)
	: CExchangeBase()
{
}


CExchangeRO::CExchangeRO(CTransaction_trakt_misc_data rec1,
												 vecTransactionDCLSTree &tree_list,
												 vecTransactionTreeAssort &tree_assort_list,
												 vecTransactionTraktSetSpc &set_spc)
	: CExchangeBase(rec1,tree_list,tree_assort_list,set_spc)
{
}

CExchangeRO::CExchangeRO(CTransaction_trakt_misc_data rec1,
												 vecTransactionDCLSTree &tree_list,
												 vecTransactionTraktSetSpc &set_spc,
												 vecTransactionTreeAssort &tree_assort_list,
												 vecTransaction_exchange_simple &exch_data)
	: CExchangeBase(rec1,tree_list,set_spc,tree_assort_list,exch_data)
{
}

CExchangeRO::~CExchangeRO(void)
{
	POSITION pos = m_listDCLSClasses_ExchDiam.GetHeadPosition();
	while (pos)
	{
		if (pos)
		{
			_dcls_classes *dcls = m_listDCLSClasses_ExchDiam.GetNext(pos);
			if (dcls != NULL)
				delete dcls;
		}
	}
}

// PUBLIC
// Do the actual calculation of exchange, according to R.Ollas; 070525 p�d
void CExchangeRO::doExchangeCalculation(void)
{
	CString S;
	int nTraktID;
	int nTreeID;
	double fDBH_middle = 0.0;	// "Klassmitt"
	double fBark = 0.0;				// "Barkavdrag"
	double fDBHub = 0.0;			// DBH under bark
	double fMinDiam = 0.0;
	double fHgt = 0.0;
	double fExchValue = 0.0;
	double fExchVolume = 0.0;
	double fPrevExchVolume = 0.0;
	double fM3Ub = 0.0;
	double fAssPrice = 0.0;
	double fAssPricePerSpc = 0.0;
	double fM3FubToM3TO = 0.0;
	int nPulpwoodType;
	int nPriceIn;		
	int nActiveSpcID = -1;
	long nNumOfTreesPerSpc = 0;
	CTransaction_assort *pPrlAss;
	CTransaction_dcls_tree recTree;

	double fSUM = 0.0;
	double fVolume = 0.0;

	CTransaction_assort *pAss2;
	_dcls_classes *dcls;

	// Add this CList to add vecPrlAss data and sort by Min. diameter; 070528 p�d
	ListPrlAss listPrlAssort;
	PrlAssSorter sortPrlAss;
	
	vecTransactionAssort vecPrlAss = getPricelistAssortment();
	
	vecTransactionDCLSTree vecTrees = getTrees();

	for (UINT i = 0;i < vecPrlAss.size();i++)
	{
		listPrlAssort.AddTail(new CTransaction_assort(vecPrlAss[i]));
	}

//===================================================================================================

	// Make sure we have some data to work with; 070525 p�d
	if (listPrlAssort.GetCount() > 0)
	{
	
		sortPrlAss.SortPointerCollection(listPrlAssort,CDCLSCompare_PrlAss());
			
		// Step 1) Do exchange calculations for each specie per assortment
		// and order them per exchange diameter; 070528 p�d
		m_listDCLSClasses_ExchDiam.RemoveAll();	// Clear list
		nNumOfTreesPerSpc = 0;
		POSITION pos1 = listPrlAssort.GetHeadPosition();
		while (pos1)
		{
			pPrlAss = listPrlAssort.GetNext(pos1);
			// Set info. from Assortments; 070525 p�d
			fMinDiam = pPrlAss->getMinDiam();
			// Load Pricelist information for specie.
			// OBS! Only do this if selected type is Pricelist, not avg. assortment; 070608 p�d

			if (m_recMiscData.getTypeOf() == 1) // Pricelist
			{
				loadPrlPriceAndQescForSpc(pPrlAss->getSpcID());
				nActiveSpcID = pPrlAss->getSpcID();
			}

			for (long ii = 0;ii < vecTrees.size();ii++)
			{
				recTree = vecTrees[ii];
				
				fAssPrice = 0.0;
				fPrevExchVolume = 0.0;
				// Only add assortments with a Min.Diamter > 0.0
				// Otherwise, assortment is used only for "�verf�ringar"
				if (fMinDiam > 0.0)
				{
					nPulpwoodType = pPrlAss->getPulpType();
					// Calculate per specie; 070525 p�d

					if (recTree.getSpcID() == pPrlAss->getSpcID())
					{
						// Load Pricelist information for specie. Also make a check that
						// this information isn't alrady loaded (check specie id); 070529 p�d
						// OBS! Only do this if selected type is Pricelist, not avg. assortment; 070608 p�d
						if (m_recMiscData.getTypeOf() == 1) // Pricelist
						{
							if (nActiveSpcID != recTree.getSpcID())
							{
								loadPrlPriceAndQescForSpc(recTree.getSpcID());
							} // if (nActiveSpcID != recTree.getSpcID())
							//nActiveSpcID = recTree.getSpcID();
						}	// if (m_recMiscData.getTypeOf() == 1)

						if (m_nTimberpriceIn == 1)	// Price in m3fub; 080424 p�d
							nPriceIn = 3;
						else if (m_nTimberpriceIn == 2)	// Price in m3to; 080424 p�d
							nPriceIn = 0;
						else
							nPriceIn = 0;	//Lets say that default price in = m3to; 080424 p�d

						fDBH_middle = getClassMid(recTree);
						fBark = recTree.getBarkThick()*2.0;	// Double bark-thickness; 090310 p�d
						fDBHub = (fDBH_middle - fBark);
						fHgt = recTree.getHgt()/10.0;	// From (dm) to (m)
						
						fM3Ub = recTree.getM3ub();
						fExchValue = getExchange(fDBHub,fHgt,fMinDiam,(nPulpwoodType == 0 ? 1 : 0),(nPulpwoodType == 1 ? 0 : 1));

						fExchVolume = fM3Ub*fExchValue;

						// Added 090604 p�d
						setM3FubForDCLSTreesList(recTree,fExchVolume);

						// Only add data if exchange volume > 0.0; 070528 p�d
						if (fExchVolume > 0.0)
						{

							// Determin what kind of assortmet this is; 070529 p�d
							// Set nPriceIn like this:
							// 0 = Price from pricelist, price set in m3to
							// 3 = Price from pricelist, price set in m3fub
							// 1 = Price set in Assortment NOT PULP
							// 2 = Price set in Assortment PULP

							// Price from pricelist = Assortment price for fub and to equals zero; 070529 p�d
							// This means that price is to be taken from Pricelist (a timber assortment); 070529 p�d
							if (pPrlAss->getPriceM3fub() == 0.0 &&
									pPrlAss->getPriceM3to() == 0.0 &&
									nPulpwoodType == 0 &&
									m_recMiscData.getTypeOf() == 1)		// Pricelist
							{

								// Adaption for "Intr�ngsv�rdering":
								// Number of trees is a combination of "Tr�d i gata (Uttag) och Kanttr�d"
								// OBS! This only allies to "Intr�ng"; 080307 p�d
								nNumOfTreesPerSpc = (recTree.getNumOf()+recTree.getNumOfRandTrees());	// Number of tress in diamterclass; 071210 p�d

#ifdef __CALCULATE_ON_BARK_UNDER_BARK
								// Get price for "klassmitt" for diamterclass
								// Diameter under bark; 090311 p�d
								if (nPriceIn == 3)	// M3Fub
									fAssPrice = calculatePrice(fDBHub);	
								// Diameter on bark; 090311 p�d
								else if (nPriceIn == 0)	// M3To
									fAssPrice = calculatePrice(fDBH_middle);	
#else
								fAssPrice = calculatePrice(fDBHub);	
								//S.Format(_T("ASSORT PRICE\nrecTree.getSpcID() %d\nPrice %f\nDBHub %f"),recTree.getSpcID(),fAssPrice,fDBHub);
								//AfxMessageBox(S);
#endif
							}	// if (pPrlAss->getPriceM3fub() == 0.0 && pPrlAss->getPriceM3to() == 0.0)
						
							// Price from "pricelist" avg. assortment prices; 070608 p�d
							// We only need a price for m3to; 071024 p�d
							else if (pPrlAss->getPriceM3to() > 0.0 && 
			 								 nPulpwoodType == 0 &&
											 m_recMiscData.getTypeOf() >= 0) // Can be either from Pricelist or Avg. assortments; fixes #2546 111114 Peter
							{
								nPriceIn = -1;	// M3To
								fAssPrice = pPrlAss->getPriceM3to();
								// Adaption for "Intr�ngsv�rdering":
								// Number of trees is a combination of "Tr�d i gata (Uttag) och Kanttr�d"
								// OBS! This only allies to "Intr�ng"; 080307 p�d
								nNumOfTreesPerSpc = (recTree.getNumOf()+recTree.getNumOfRandTrees());	// Number of tress in diamterclass; 071210 p�d
								//S.Format(_T("Avg assort TO\nfAssPrice %f"),fAssPrice);
								//AfxMessageBox(S);

							}	// if (pPrlAss->getPriceM3fub() == 0.0 && pPrlAss->getPriceM3to() == 0.0)

							// Price from assortment (use fub price) and check that it's not a Pulpwood assortment; 070529 p�d
							// Also check that the type of ("Massa (typ av ber�kning)") inte �r definierad. I.e. nPulpwoodType = 0; 070611 p�d
							else if (pPrlAss->getPriceM3fub() > 0.0 &&
											 //pPrlAss->getPriceM3to() == 0.0 && 
											 nPulpwoodType == 0 &&						// Not a Pulpwood assortment
											 m_recMiscData.getTypeOf() >= 0)	// Can be either from Pricelist or Avg. assortments
							{
								nPriceIn = -1;	// M3Fub
								fAssPrice = pPrlAss->getPriceM3fub();
								// Adaption for "Intr�ngsv�rdering":
								// Number of trees is a combination of "Tr�d i gata (Uttag) och Kanttr�d"
								// OBS! This only allies to "Intr�ng"; 080307 p�d
								nNumOfTreesPerSpc = (recTree.getNumOf()+recTree.getNumOfRandTrees());	// Number of tress in diamterclass; 071210 p�d
							}	// else if (!pPrlAss->getIsPulp() && pPrlAss->getPriceM3fub() > 0.0)

							// Price from assortment (use fub price) and check that it's a Pulpwood assortment; 070529 p�d
							// Also check that the type of ("Massa (typ av ber�kning)") inte �r definierad. I.e. Typeof = 0; 070611 p�d
							else if (pPrlAss->getPriceM3fub() > 0.0 &&
											 pPrlAss->getPriceM3to() == 0.0 && 
											 nPulpwoodType > 0 &&								// A pulpwood assortment; 1 = "Fallande l�ngder", 2 = "3 meters l�ngder"; 070611 p�d
											 m_recMiscData.getTypeOf() >= 0)		// Can be either from Pricelist or Avg. assortments
							{
								nPriceIn = 2;	// M3Fub massa
								fAssPrice = pPrlAss->getPriceM3fub();
							}	// else if (pPrlAss->getIsPulp() && pPrlAss->getPriceM3fub() > 0.0)
							_dcls_classes *pExchangeData = new _dcls_classes(recTree.getTraktID(),
																															 recTree.getTreeID(),
																															 getClassMid(recTree),
																															 fMinDiam,
																															 recTree.getSpcID(),
																															 fExchVolume,
																															 fAssPrice,
																															 getClassMid(recTree),
																															 nPriceIn,
																															 nNumOfTreesPerSpc);
							m_listDCLSClasses_ExchDiam.AddTail(pExchangeData);
							
						}	// if (fExchVolume > 0.0)
					}	// if (tree.getSpcID() == rec.getSpcID())
				}	// if (fMinDiam > 0.0)
			}	// for (UINT ii = 0;ii < vecTrees.size();ii++)
		}	// while (pos1)
		
//===================================================================================================
		m_vecTreeAssorts.clear();
		POSITION pos2 = listPrlAssort.GetHeadPosition();
		while (pos2)
		{
			pAss2 = listPrlAssort.GetNext(pos2);
			// Only add assortments that has a Mindiamter > 0.0; 070528 p�d
			if (pAss2->getMinDiam() > 0.0)
			{
				fExchVolume = 0.0;
				fAssPrice = 0.0;
				nNumOfTreesPerSpc = 0;
				fAssPricePerSpc = 0.0;
				// Get M3Fub to M3to; 090310 p�d
				fM3FubToM3TO = getM3FubToM3TO(pAss2->getSpcID());

				if (m_listDCLSClasses_ExchDiam.GetCount() > 0)
				{
					POSITION pos3 = m_listDCLSClasses_ExchDiam.GetHeadPosition();
					while (pos3)
					{
						dcls = m_listDCLSClasses_ExchDiam.GetNext(pos3);
						if (pAss2->getMinDiam() == dcls->exch_diam && pAss2->getSpcID() == dcls->spcid)
						{
							nTraktID = dcls->traktid;
							nTreeID = dcls->treeid;
							nPriceIn = dcls->price_in;
							// Total exchange volume; 090310 p�d
							fExchVolume += dcls->volume;
							// Price = 0 => In M3To for Avg. pricelist and matrix pricelist
							// Price = 1 => In M3Fub for avg pricelist
							// Price = 3 => In M3Fub for matrix-pricelist
							if (nPriceIn == 0 || nPriceIn == 1 || nPriceIn == 3)
							{
	
								nNumOfTreesPerSpc += dcls->nNumOfSpc;
								// Calculate price for Timber assortment for specie; 081217 p�d
								fAssPrice += dcls->volume*dcls->price; //*dcls->nNumOfSpc;
/*
			S.Format(_T("Pricelist\nnPriceIn %d\npAss2->getAssortName() %s\npAss2->getSpcID() %d\ndcls->spcid %d\npAss2->getMinDiam() %f\ndcls->exch_diam %f\ndcls->volume %f\ndcls->price %f"),
					nPriceIn,pAss2->getAssortName(),pAss2->getSpcID(),dcls->spcid,pAss2->getMinDiam(),dcls->exch_diam,dcls->volume,dcls->price);
			AfxMessageBox(S);
*/
							}
							else
							{
								nNumOfTreesPerSpc = 1;
								fAssPrice = dcls->price;
							}

						}	// if (pAss2->getMinDiam() == dcls->exch_diam() && pAss2->getSpcID() == dcls->spcid)
					}	// while (pos3)
						
					// Changed 2009-11-05 p�d
					// Open this method, calcualte m3fub (exchangevolume), without the need
					// to have a price "fAssPrice"; 091105 p�d
					if (fExchVolume > 0.0)	
					//if (fExchVolume > 0.0 && fAssPrice > 0.0)
					//if (fDCLSPrice > 0.0 && nNumOfDCLSPerSpc > 0)
					{
	
						if (m_recMiscData.getTypeOf() == 1)	// Pricelist
						{
							// nPriceIn = 0 => Price in M3TO
							// nPriceIn = 3 => Price in M3FUB
							if (nPriceIn == 0)
								fAssPricePerSpc = fAssPrice*fM3FubToM3TO;
							else	// Also for nPriceIn == 3 (M3Fub); 090311 p�d
								fAssPricePerSpc = fAssPrice;
						}
						else if (m_recMiscData.getTypeOf() == 2)	// Avg. pricelist
						{
							// nPriceIn = 0 => Price in M3TO
							// nPriceIn = 3 => Price in M3FUB
							if (nPriceIn == 0)
								fAssPricePerSpc = fAssPrice*fM3FubToM3TO;
							else	// Also for nPriceIn == 3 (M32Fub); 090311 p�d
								fAssPricePerSpc = fAssPrice;
						}

/*
								S.Format(_T("Pricelist\nSpc %d\npAss2->getAssortName() %s\nfExchVolume %f\nfAssPricePerSpc %f\nfAssPrice %f"),
									pAss2->getSpcID(),pAss2->getAssortName(),fExchVolume,fAssPricePerSpc,fAssPrice);
								AfxMessageBox(S);
*/

						m_vecTreeAssorts.push_back(CTransaction_tree_assort(nTraktID,
																																nTreeID,
																																pAss2->getMinDiam(),
																																pAss2->getSpcID(),
																																getSpcName(pAss2->getSpcID()),
																																pAss2->getAssortName(),
																																fExchVolume,
																																fAssPricePerSpc,
																																nPriceIn));
						fAssPrice = 0.0;
						fAssPricePerSpc = 0.0;

					}
				}	// if (m_listDCLSClasses_ExchDiam.GetCount() > 0)
			}	// if (pAss2->getMinDiam() > 0.0)
		}	// while (pos2)

	}	// if (vecPrlAss.size() > 0 && vecTrees.size() > 0)
	
//===================================================================================================
	if (m_vecTreeAssorts.size() > 0 && getPricelistSpecies().size() > 0)
	{
		std::sort(m_vecTreeAssorts.begin(),m_vecTreeAssorts.end(),SortData);
		for (UINT i = 0;i < getPricelistSpecies().size();i++)
		{
			fExchVolume = 0.0;
			fPrevExchVolume = 0.0;
			for (UINT ii = 0;ii < m_vecTreeAssorts.size();ii++)
			{
				if (m_vecTreeAssorts[ii].getSpcID() == getPricelistSpecies()[i].getSpcID())
				{
					fExchVolume = m_vecTreeAssorts[ii].getExchVolume();
					m_vecTreeAssorts[ii].setExchVolume(fExchVolume - fPrevExchVolume);

					fPrevExchVolume = fExchVolume;
				}
			}
		}	// for (UINT i = 0;i < m_vecPricelistSpecies.size();i++)
	}
//}
//===================================================================================================

	// Clear up listPrlAssort
	POSITION pos4 = listPrlAssort.GetHeadPosition();
	while (pos4)
	{
		CTransaction_assort *pAss4 = listPrlAssort.GetNext(pos4);
		if (pAss4 != NULL)
			delete pAss4;
	}
}

// Only do Exchgange calculation, no pricecalc. involved; 100409 p�d
void CExchangeRO::doExchangeCalculationSimple(void)
{
	long nNumOfTreesPerSpc = 0;
	CTransaction_assort *pPrlAss;
	double fMinDiam = 0.0;
	CString sAssName,S;
	int	nTraktID;
	int nTreeID;
	int nPriceIn;
	int nActiveSpcID = -1;
	CTransaction_dcls_tree recTree;
	double fPrevExchVolume = 0.0;
	int nPulpwoodType;
	double fDBH_middle = 0.0;	// "Klassmitt"
	double fBark = 0.0;				// "Barkavdrag"
	double fDBHub = 0.0;			// DBH under bark
	double fM3Ub = 0.0;
	double fHgt = 0.0;
	double fExchValue = 0.0;
	double fExchVolume = 0.0;

	// Add this CList to add vecPrlAss data and sort by Min. diameter; 070528 p�d
	ListPrlAss listPrlAssort;
	PrlAssSorter sortPrlAss;

	CTransaction_assort *pAss2;
	_dcls_classes *dcls;
	
	vecTransactionAssort vecPrlAss = getPricelistAssortment();
	
	vecTransactionDCLSTree vecTrees = getTrees();

	for (UINT i = 0;i < vecPrlAss.size();i++)
	{
		listPrlAssort.AddTail(new CTransaction_assort(vecPrlAss[i]));
	}

//===================================================================================================

	// Make sure we have some data to work with; 070525 p�d
	if (listPrlAssort.GetCount() > 0)
	{
		sortPrlAss.SortPointerCollection(listPrlAssort,CDCLSCompare_PrlAss());
				
		// Step 1) Do exchange calculations for each specie per assortment
		// and order them per exchange diameter; 070528 p�d
		m_listDCLSClasses_ExchDiam.RemoveAll();	// Clear list
		nNumOfTreesPerSpc = 0;
		POSITION pos1 = listPrlAssort.GetHeadPosition();
		while (pos1)
		{
			pPrlAss = listPrlAssort.GetNext(pos1);
			// Set info. from Assortments; 070525 p�d
			fMinDiam = pPrlAss->getMinDiam();
			sAssName = pPrlAss->getAssortName();
			// Load Pricelist information for specie.
			// OBS! Only do this if selected type is Pricelist, not avg. assortment; 070608 p�d

			if (m_recMiscData.getTypeOf() == 1) // Pricelist
			{
				loadPrlPriceAndQescForSpc(pPrlAss->getSpcID());
				nActiveSpcID = pPrlAss->getSpcID();
			}

			for (long ii = 0;ii < vecTrees.size();ii++)
			{
				recTree = vecTrees[ii];
				
				fDBH_middle = 0.0;	// "Klassmitt"
				fBark = 0.0;				// "Barkavdrag"
				fDBHub = 0.0;			// DBH under bark
				fM3Ub = 0.0;
				fHgt = 0.0;
				fExchValue = 0.0;
				fExchVolume = 0.0;

				fPrevExchVolume = 0.0;
				// Only add assortments with a Min.Diamter > 0.0
				// Otherwise, assortment is used only for "�verf�ringar"
				if (fMinDiam > 0.0)
				{
					nPulpwoodType = pPrlAss->getPulpType();
					// Calculate per specie; 070525 p�d

					if (recTree.getSpcID() == pPrlAss->getSpcID())
					{
						// Load Pricelist information for specie. Also make a check that
						// this information isn't alrady loaded (check specie id); 070529 p�d
						// OBS! Only do this if selected type is Pricelist, not avg. assortment; 070608 p�d
						if (m_recMiscData.getTypeOf() == 1) // Pricelist
						{
							if (nActiveSpcID != recTree.getSpcID())
							{
								loadPrlPriceAndQescForSpc(recTree.getSpcID());
							} // if (nActiveSpcID != recTree.getSpcID())
							//nActiveSpcID = recTree.getSpcID();
						}	// if (m_recMiscData.getTypeOf() == 1)

						fDBH_middle = getClassMid(recTree);
						fBark = recTree.getBarkThick()*2.0;	// Double bark-thickness; 090310 p�d
						fDBHub = (fDBH_middle - fBark);
						fHgt = recTree.getHgt()/10.0;	// From (dm) to (m)
						
						fM3Ub = recTree.getM3ub();
						fExchValue = getExchange(fDBHub,fHgt,fMinDiam,(nPulpwoodType == 0 ? 1 : 0),(nPulpwoodType == 1 ? 0 : 1));

						fExchVolume = fM3Ub*fExchValue;

						m_vecExchSimpleData.push_back(CTransaction_exchange_simple(recTree.getTraktID(),recTree.getSpcID(),recTree.getSpcName(),sAssName,
																																			 fDBH_middle,fBark,fDBHub,fMinDiam,recTree.getDCLS_from(),recTree.getDCLS_to(),
																																			 fExchValue,fExchVolume,0.0,nPulpwoodType));
							if (fExchVolume > 0.0)
							{
								_dcls_classes *pExchangeData = new _dcls_classes(recTree.getTraktID(),
																															 recTree.getTreeID(),
																															 getClassMid(recTree),
																															 fMinDiam,
																															 recTree.getSpcID(),
																															 fExchVolume,
																															 0.0,
																															 getClassMid(recTree),
																															 nPriceIn,
																															 nNumOfTreesPerSpc);
								m_listDCLSClasses_ExchDiam.AddTail(pExchangeData);
							}
/*
						S.Format(_T("CExchangeRO::doExchangeCalculationSimpleSpc %d MinDiam %f AssName %s Diam %f fM3Ub %f fExchValue %f fExchVolume %f nPulpwoodType %d\n"),
							recTree.getSpcID(),
							fMinDiam,
							sAssName,
							recTree.getDCLS_from(),
							fM3Ub,
							fExchValue,
							fExchVolume,
							nPulpwoodType);
						OutputDebugString(S);
*/

					}	// if (tree.getSpcID() == rec.getSpcID())
				}	// if (fMinDiam > 0.0)
			}	// for (UINT ii = 0;ii < vecTrees.size();ii++)
		}	// while (pos1)
		
//===================================================================================================
		m_vecTreeAssorts.clear();
		POSITION pos2 = listPrlAssort.GetHeadPosition();
		while (pos2)
		{
			pAss2 = listPrlAssort.GetNext(pos2);
			// Only add assortments that has a Mindiamter > 0.0; 070528 p�d
			if (pAss2->getMinDiam() > 0.0)
			{
				if (m_listDCLSClasses_ExchDiam.GetCount() > 0)
				{
					POSITION pos3 = m_listDCLSClasses_ExchDiam.GetHeadPosition();
					while (pos3)
					{
						dcls = m_listDCLSClasses_ExchDiam.GetNext(pos3);
						if (pAss2->getMinDiam() == dcls->exch_diam && pAss2->getSpcID() == dcls->spcid)
						{
							nTraktID = dcls->traktid;
							nTreeID = dcls->treeid;
							nPriceIn = dcls->price_in;
						}	// if (pAss2->getMinDiam() == dcls->exch_diam() && pAss2->getSpcID() == dcls->spcid)
					}	// while (pos3)
						
						m_vecTreeAssorts.push_back(CTransaction_tree_assort(nTraktID,
																																nTreeID,
																																pAss2->getMinDiam(),
																																pAss2->getSpcID(),
																																getSpcName(pAss2->getSpcID()),
																																pAss2->getAssortName(),
																																0.0,
																																0.0,
																																nPriceIn));
				}	// if (m_listDCLSClasses_ExchDiam.GetCount() > 0)
			}	// if (pAss2->getMinDiam() > 0.0)
		}	// while (pos2)
	}	// if (vecPrlAss.size() > 0 && vecTrees.size() > 0)

}

// PROTECTED

// PRIVATE
double CExchangeRO::calcExchange(double dbh,double hgt,double min_diam)
{
  double fDBH = dbh;			// DBH in m_vecDCLSTrees
  double fHgt = hgt;			// Height of tree, in m_vecDCLSTrees
  double fMinDiam = min_diam;	// Min diamter for exchange set for assortment, in Pricelist (from mm to cm)
  double fValue;		// Calculated value according to R.Ollas

  //--------------------------------------------------------
  //  Check that there's no value equals 0.0; 030325 p�d
  //--------------------------------------------------------
  if (fDBH == 0.0 || fHgt == 0.0)
    return 0.0;

  //--------------------------------------------------------
  //  Check if dbh and min diam = 0, div. by 0; 020517 p�d
  //--------------------------------------------------------
  if ((fDBH - fMinDiam) == 0.0)
    return 0.0;

  //--------------------------------------------------------
  //  Check if dbh < min diam , return 0.0;
  //--------------------------------------------------------
  if (fDBH < fMinDiam)
    return 0.0;

  fValue = (1.0 - (0.842/(fDBH - fMinDiam)) + (58.2/(fDBH*fHgt)) - ((8.7*fMinDiam)/(fDBH*fHgt)));
  //--------------------------------------------------------
  //  Check that value is 0.0 < fValue < 1.0
  //  If fValue < 0.0 return 0.0
  //  If fValue > 1.0 return 1.0
  //--------------------------------------------------------
  if (fValue > 1.0) return 1.0;
  else if (fValue < 0.0) return 0.0;
  else return fValue;

	return 0.0;
}

double CExchangeRO::calcGFall(double dbh,double hgt,double min_diam)
{
  double fDBH = dbh;
  double fHgt = hgt;
  double fMinDiam = min_diam;	// From mm to cm
  double fGFall;
  //--------------------------------------------------------
  //  Check that there's no value equals 0.0; 030325 p�d
  //--------------------------------------------------------
  if (fDBH == 0.0 || fHgt == 0.0)
    return 0.0;

  //--------------------------------------------------------
  //  Check if dbh and min diam = 0, div. by 0; 060524 p�d
  //--------------------------------------------------------
  if ((fDBH - fMinDiam) == 0.0)
    return 0.0;

  //--------------------------------------------------------
  //  Check if dbh < min diam , return 0.0;
  //--------------------------------------------------------
  if (fDBH < fMinDiam)
    return 0.0;

  fGFall = (1.0 - 0.57/(fDBH - fMinDiam) - 5.0/(fDBH*fHgt));

  //--------------------------------------------------------
  //  Check that value is 0.0 < fGFall < 1.0
  //  If fGFall < 0.0 return 0.0
  //  If fGFall > 1.0 return 1.0
  //--------------------------------------------------------
  if (fGFall > 1.0) return 1;
  else if (fGFall < 0.0) return 0.0;
  else return fGFall;


	return 0.0;
}

double CExchangeRO::calcG3m(double dbh,double hgt,double min_diam)
{
  double fDBH = dbh;
  double fHgt = hgt;
  double fMinDiam = min_diam;	// From mm to cm
  double fG3m;

  //--------------------------------------------------------
  //  Check that there's no value equals 0.0; 030325 p�d
  //--------------------------------------------------------
  if (fDBH == 0.0 || fHgt == 0.0)
    return 0.0;

  //--------------------------------------------------------
  //  Check if dbh and min diam = 0, div. by 0; 060524 p�d
  //--------------------------------------------------------
  if ((fDBH - fMinDiam) == 0.0)
    return 0.0;

  //--------------------------------------------------------
  //  Check if dbh < min diam , return 0.0;
  //--------------------------------------------------------
  if (fDBH < fMinDiam)
    return 0.0;

  fG3m = (1 - 0.37/(fDBH - fMinDiam) + 1.15/(fDBH*fHgt));
  //--------------------------------------------------------
  //  Check that value is 0.0 < fG3m < 1.0
  //  If fG3m < 0.0 return 0.0
  //  If fG3m > 1.0 return 1.0
  //--------------------------------------------------------
  if (fG3m > 1.0) return 1;
  else if (fG3m < 0.0) return 0.0;
  else return fG3m;

	return 0.0;
}

//================================================================
//  Ollars exchange calcualtion
//  dbh				= diameter at breast height (under bark)
//  min_diam	= min. diamter for assortment (under bark)
//  hgt				= tree height
//  gval			= Calculate 1 = "Timmerandel", 0 = "Massandel"
//  sel				= If gval = 0, then 0 = GFall and 1 = G3m
//================================================================
double CExchangeRO::getExchange(double dbh,double hgt,double min_diam,int gval,int sel)
{
  double fDBH      = dbh/10.0;	// (mm) to (cm)
  double fHgt      = hgt;
  double fMinDiam  = min_diam;
  int nGVal        = gval;    //  1 = "Timmerandel", 0 = "Massa-andel"
  int nSel         = sel;     //  For "Massandel" 0 = GFall, 1 = G3m
	CString S;
  //--------------------------------------------------
  //  First, chech
  //--------------------------------------------------
  if (nGVal == 1)
  {
    return calcExchange(fDBH,fHgt,fMinDiam);
  }
  else if (nGVal == 0)
  {
    if (nSel == 0) // GFall
    {
			return calcGFall(fDBH,fHgt,fMinDiam);
    }
    else if (nSel == 1)  // G3m
    {
      return calcG3m(fDBH,fHgt,fMinDiam);
    }
  }

  return 0.0; // Nothing
}
